package com.silensoft.conflated.data.service.column;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class ColumnTests {
    final Column column = new Column();

    @Test
    void testGettersAndSetters() {
        // WHEN
        column.setName("name");
        column.setType("measure");
        column.setExpression("expr");

        // THEN
        assertEquals(column.getName(), "name");
        assertEquals(column.getType(), "measure");
        assertEquals(column.getExpression(), "expr");
    }

    @Test
    void testGetColumnAsSql() {
        // GIVEN
        column.setName("failedCallsRatio");
        column.setExpression("failedCalls / allCalls");

        // WHEN
        final String columnAsSql = column.getColumnAsSql();

        // THEN
        assertEquals(columnAsSql, "failedCalls / allCalls AS failedCallsRatio");
    }

    @Test
    void testGetTimeSortBySubqueryPrefix() {
        // GIVEN
        column.setName("failedCalls");

        // WHEN
        final String timeSortBySubqueryPrefix = column.getTimeSortBySubqueryPrefix();

        // THEN
        assertEquals(timeSortBySubqueryPrefix, "failedCalls in ( SELECT failedCalls FROM ( ");
    }

    @Test
    void testGetSqlGroupByExpression() {
        // GIVEN
        column.setName("failedCalls");
        column.setExpression("");

        // WHEN
        final String sqlGroupByExpression = column.getSqlGroupByExpression(0);
        final String sqlGroupByExpression2 = column.getSqlGroupByExpression(1);

        // THEN
        assertEquals(sqlGroupByExpression, "failedCalls");
        assertEquals(sqlGroupByExpression2, ", failedCalls");
    }
}
