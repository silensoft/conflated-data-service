package com.silensoft.conflated.data.service.types;

import java.util.List;

public interface ListConsumer<T> {
    void consume(final List<T> values);
}
