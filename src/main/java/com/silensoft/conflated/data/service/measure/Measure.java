package com.silensoft.conflated.data.service.measure;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class Measure {
    private @NotNull(message = "name is mandatory field") String name;

    public String getName() {
        return name;
    }

    public void setName(final String newName) {
        name = newName;
    }
}

