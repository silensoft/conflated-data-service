package com.silensoft.conflated.data.service.column;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class Column {
    private @NotNull(message = "name is mandatory field") String name;

    private @NotNull(message = "expression is mandatory field") String expression;

    private @NotNull(message = "type is mandatory field") String type;

    public String getName() {
        return name;
    }

    public void setName(final String newName) {
        name = newName;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(final String newExpression) {
        expression = newExpression;
    }

    public String getType() {
        return type;
    }

    public void setType(final String newType) {
        type = newType;
    }

    public String getColumnAsSql() {
        final StringBuilder sqlBuilder = new StringBuilder(128);

        if (!expression.isEmpty()) {
            sqlBuilder.append(expression);
            sqlBuilder.append(" AS ");
        }

        sqlBuilder.append(name);
        return sqlBuilder.toString();
    }

    public String getTimeSortBySubqueryPrefix() {
        return name + " in ( SELECT " + name + " FROM ( ";
    }

    public String getSqlGroupByExpression(final int columnIndex) {
        final StringBuilder sqlGroupByExpressionBuilder = new StringBuilder(128);

        if (columnIndex != 0) {
            sqlGroupByExpressionBuilder.append(", ");
        }

        sqlGroupByExpressionBuilder.append(expression.isEmpty() ? getName() : expression);
        return sqlGroupByExpressionBuilder.toString();
    }
}
