package com.silensoft.conflated.data.service.dimension;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Stateless
public class DimensionMetadataFetcherImpl implements DimensionMetadataFetcher {
    @SuppressWarnings({"LocalVariableOfConcreteClass"})
    @Override
    public List<Dimension> fetchDimensions(final Connection connection, final String sqlStatement) throws SQLException {
        final ResultSetHandler<List<Dimension>> resultSetHandler = resultSet -> {
            if (!resultSet.next()) {
                return Collections.emptyList();
            }

            final ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            final int columnCount = resultSetMetaData.getColumnCount();
            final List<Dimension> dimensions = new ArrayList<>(columnCount);

            for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
                final int columnType = resultSetMetaData.getColumnType(columnIndex + 1);
                if (columnType == Types.FLOAT || columnType == Types.DOUBLE || columnType == Types.DECIMAL) {
                    continue;
                }

                //noinspection ObjectAllocationInLoop
                final Dimension dimension = new Dimension();
                dimension.setName(resultSetMetaData.getColumnName(columnIndex + 1));
                dimension.setDate(columnType == Types.DATE);
                dimension.setTimestamp(columnType == Types.TIMESTAMP || columnType == Types.TIMESTAMP_WITH_TIMEZONE);
                final String columnClassName = resultSetMetaData.getColumnClassName(columnIndex + 1);
                dimension.setString(String.class.getName().equals(columnClassName));
                dimensions.add(dimension);
            }

            return dimensions;
        };

        final QueryRunner queryRunner = new QueryRunner();
        return queryRunner.query(connection, sqlStatement, resultSetHandler);
    }
}
