package com.silensoft.conflated.data.service.dataquery;

import com.silensoft.conflated.data.service.Constants;
import com.silensoft.conflated.data.service.column.Column;
import com.silensoft.conflated.data.service.datasource.DataSource;
import com.silensoft.conflated.data.service.filter.Filter;
import com.silensoft.conflated.data.service.sortby.SortBy;
import com.silensoft.conflated.data.service.types.OptionalUtils;

import javax.inject.Inject;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodReturnOfConcreteClass", "AssignmentOrReturnOfFieldWithMutableType", "MethodParameterOfConcreteClass"})
@Entity
public class DataQuery {
    @Inject
    private TimeSortQuerySqlStatementFactory timeSortQuerySqlStatementFactory;

    private @NotNull(message = "dataSource is mandatory field") DataSource dataSource;

    private @NotNull(message = "columns is mandatory field") List<Column> columns;

    private @NotNull(message = "filters is mandatory field") List<Filter> filters;

    private @NotNull(message = "sortBys is mandatory field") List<SortBy> sortBys;

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(final DataSource newDataSource) {
        dataSource = newDataSource;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(final List<Column> newColumns) {
        columns = newColumns;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(final List<Filter> newFilters) {
        filters = newFilters;
    }

    public List<SortBy> getSortBys() {
        return sortBys;
    }

    public void setSortBys(final List<SortBy> newSortBys) {
        sortBys = newSortBys;
    }

    public String getDataQuerySqlStatement() {
        final StringBuilder dataQuerySqlStatementBuilder = new StringBuilder(1024);

        dataQuerySqlStatementBuilder.append("SELECT ");
        dataQuerySqlStatementBuilder.append(getSelectedColumnsAsSql());
        dataQuerySqlStatementBuilder.append(getSqlFromAndWhereParts());
        dataQuerySqlStatementBuilder.append(getSqlGroupByPart());
        dataQuerySqlStatementBuilder.append(getSqlHavingPart());
        dataQuerySqlStatementBuilder.append(getSqlOrderByPart());

        return dataQuerySqlStatementBuilder.toString();
    }

    public String getSelectedColumnsAsSql() {
        final StringBuilder sqlBuilder = new StringBuilder(1024);
        int columnIndex = 0;

        for (final Column column : columns) {
            if (columnIndex != 0) {
                sqlBuilder.append(", ");
            }

            sqlBuilder.append(column.getColumnAsSql());
            columnIndex++;
        }

        return sqlBuilder.toString();
    }

    public String getSqlFromAndWhereParts() {
        final String sqlFromAndStaticWhereParts = dataSource.getSqlFromAndStaticWhereParts();
        final StringBuilder sqlFromAndStaticAndDynamicWhereParts = new StringBuilder(sqlFromAndStaticWhereParts);

        if (getWhereFilterCount() > 0L) {
            if (dataSource.getShouldCreateView()) {
                sqlFromAndStaticAndDynamicWhereParts.append(" WHERE (");
            } else {
                if (dataSource.hasStaticSqlWhereExpression()) {
                    sqlFromAndStaticAndDynamicWhereParts.append(" AND (");
                } else {
                    sqlFromAndStaticAndDynamicWhereParts.append(" WHERE (");
                }
            }

            final String filtersSqlWhereExpression = getFiltersSqlWhereExpression();
            sqlFromAndStaticAndDynamicWhereParts.append(filtersSqlWhereExpression);
            sqlFromAndStaticAndDynamicWhereParts.append(getSqlSortBysWhereExpression(sqlFromAndStaticWhereParts, filtersSqlWhereExpression));
            sqlFromAndStaticAndDynamicWhereParts.append(')');
        }

        return sqlFromAndStaticAndDynamicWhereParts.toString();
    }

    public long getWhereFilterCount() {
        return filters.stream().filter(filter -> Constants.DIMENSION.equals(filter.getType()) || "measure-non-aggregated".equals(filter.getType())).count();
    }

    public String getSqlGroupByPart() {
        final StringBuilder sqlGroupByPartBuilder = new StringBuilder(256);

        if (getDimensionCount() > 0L) {
            sqlGroupByPartBuilder.append(" GROUP BY ");
            sqlGroupByPartBuilder.append(getSqlGroupByExpression());
        }

        return sqlGroupByPartBuilder.toString();
    }

    public String getSqlHavingPart() {
        final StringBuilder sqlHavingPartBuilder = new StringBuilder(512);

        if (getSqlHavingFilterCount() > 0L) {
            sqlHavingPartBuilder.append(" HAVING ");
            sqlHavingPartBuilder.append(getSqlHavingExpression());
        }

        return sqlHavingPartBuilder.toString();
    }

    public String getSqlOrderByPart() {
        final StringBuilder sqlOrderByPartBuilder = new StringBuilder(512);

        if (!sortBys.isEmpty()) {
            sqlOrderByPartBuilder.append(" ORDER BY ");
            sqlOrderByPartBuilder.append(getSqlOrderByExpression());
        }

        return sqlOrderByPartBuilder.toString();
    }

    private String getSqlOrderByExpression() {
        final StringBuilder sqlOrderByExpressionBuilder = new StringBuilder(256);
        int sortByIndex = 0;
        final List<SortBy> sortBysWithoutSortByTime = sortBys.stream().filter(sortBy -> !Constants.TIME.equals(sortBy.getType())).collect(Collectors.toList());

        for (final SortBy sortBy : sortBysWithoutSortByTime) {
            sortBy.getSqlOrderByExpression(sortByIndex);
            sortByIndex++;
        }

        return sqlOrderByExpressionBuilder.toString();
    }

    private String getSqlHavingExpression() {
        final StringBuilder sqlHavingExpressionBuilder = new StringBuilder(256);
        int filterIndex = 0;
        final List<Filter> measureFilters = filters.stream().filter(filter -> Constants.MEASURE.equals(filter.getType())).collect(Collectors.toList());

        for (final Filter filter : measureFilters) {
            sqlHavingExpressionBuilder.append(filter.getSqlHavingExpression(filterIndex));
            filterIndex++;
        }

        return sqlHavingExpressionBuilder.toString();
    }

    private long getSqlHavingFilterCount() {
        return columns.stream().filter(column -> Constants.MEASURE.equals(column.getType())).count();
    }

    private String getSqlGroupByExpression() {
        final StringBuilder sqlGroupByExpressionBuilder = new StringBuilder(256);
        int columnIndex = 0;
        final List<Column> dimensionColumns = columns.stream().filter(column -> Constants.DIMENSION.equals(column.getType())).collect(Collectors.toList());

        for (final Column column : dimensionColumns) {
            sqlGroupByExpressionBuilder.append(column.getSqlGroupByExpression(columnIndex));
            columnIndex++;
        }

        return sqlGroupByExpressionBuilder.toString();
    }

    private long getDimensionCount() {
        return columns.stream().filter(column -> Constants.DIMENSION.equals(column.getType())).count();
    }

    private String getFiltersSqlWhereExpression() {
        final StringBuilder filtersSqlWhereExpression = new StringBuilder(1024);
        int filterIndex = 0;

        for (final Filter filter : filters) {
            filtersSqlWhereExpression.append(filter.getSqlWhereExpression(filterIndex));
            filterIndex++;
        }

        return filtersSqlWhereExpression.toString();
    }

    private String getSqlSortBysWhereExpression(final String sqlFromAndStaticWhereParts, final String filtersSqlWhereExpression) {
        final StringBuilder sqlSortBysWhereExpression = new StringBuilder(1024);

        if (hasTimeSort()) {
            if (!filtersSqlWhereExpression.isEmpty()) {
                sqlSortBysWhereExpression.append(" AND ");
            }

            sqlSortBysWhereExpression.append('(');
            sqlSortBysWhereExpression.append(getTimeSortSqlWhereExpression(sqlFromAndStaticWhereParts));
            sqlSortBysWhereExpression.append(')');
        }

        return sqlSortBysWhereExpression.toString();
    }

    private boolean hasTimeSort() {
        return sortBys.stream().anyMatch(sortBy -> Constants.TIME.equals(sortBy.getType()));
    }

    private String getTimeSortSqlWhereExpression(final String sqlFromAndStaticWhereParts) {
        final StringBuilder timeSortSqlWhereExpressionBuilder = new StringBuilder(1024);
        final Optional<Column> firstDimensionColumn = getNthColumnOfType(1L, Constants.DIMENSION);
        final Optional<Column> secondDimensionColumn = getNthColumnOfType(2L, Constants.DIMENSION);
        final Optional<Column> firstMeasureColumn = getNthColumnOfType(1L, Constants.MEASURE);

        sortBys.stream().filter(sortBy -> Constants.TIME.equals(sortBy.getType())).findFirst().ifPresent(timeSortBy ->
                OptionalUtils.ifAllPresent(Arrays.asList(firstDimensionColumn, secondDimensionColumn, firstMeasureColumn), (timeSortByColumns -> {
                    timeSortSqlWhereExpressionBuilder.append(timeSortByColumns.get(0).getTimeSortBySubqueryPrefix());
                    timeSortSqlWhereExpressionBuilder.append(timeSortQuerySqlStatementFactory.createTimeSortQuerySqlStatement(
                            sqlFromAndStaticWhereParts,
                            timeSortBy,
                            timeSortByColumns.get(0),
                            timeSortByColumns.get(1),
                            timeSortByColumns.get(2)));

                    timeSortSqlWhereExpressionBuilder.append(" ) as subquery");
                })));

        return timeSortSqlWhereExpressionBuilder.toString();
    }

    private Optional<Column> getNthColumnOfType(final long nth, final String columnType) {
        return columns.stream().filter(column -> columnType.equals(column.getType())).skip(nth - 1L).findFirst();
    }
}
