package com.silensoft.conflated.data.service.dataquery;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@SuppressWarnings("MethodParameterOfConcreteClass")
public interface DataQueryExecutor {
    Map<String, List<Object>> executeQuery(final Connection connection, final DataQuery dataQuery) throws SQLException;
}
