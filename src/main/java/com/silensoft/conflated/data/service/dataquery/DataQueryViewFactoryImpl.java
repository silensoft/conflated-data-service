package com.silensoft.conflated.data.service.dataquery;

import com.silensoft.conflated.data.service.datasource.DataSource;
import org.apache.commons.dbutils.QueryRunner;

import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

@Stateless
public class DataQueryViewFactoryImpl implements DataQueryViewFactory {
    private static final Logger logger = Logger.getLogger("DataQueryViewFactoryImpl");

    @SuppressWarnings({"FeatureEnvy", "MethodParameterOfConcreteClass"})
    @Override
    public void createDataQueryViewIfNeeded(final Connection connection, final DataSource dataSource) throws SQLException {
        if (!dataSource.getShouldCreateView()) {
            return;
        }

        final DatabaseMetaData databaseMetaData = connection.getMetaData();

        try (final ResultSet conflatedSchemas = databaseMetaData.getSchemas(null, "CONFLATED___")) {
            if (!conflatedSchemas.next()) {
                final QueryRunner queryRunner = new QueryRunner();
                queryRunner.execute(connection, "CREATE SCHEMA Conflated___");
            }
        }

        try (final ResultSet dataSourceViews = databaseMetaData.getTables(null, "CONFLATED___", dataSource.getName(), new String[]{"VIEW"})) {
            if (!dataSourceViews.next()) {
                final QueryRunner queryRunner = new QueryRunner();
                final String createViewSqlStatement = "CREATE VIEW Conflated___.\"" + dataSource.getName() + "\" AS " + dataSource.getSqlStatement();
                queryRunner.update(connection, createViewSqlStatement);
            }
        }
    }
}
