package com.silensoft.conflated.data.service.connection;

import com.silensoft.conflated.data.service.datasource.DataSource;

import java.sql.Connection;
import java.sql.SQLException;

@SuppressWarnings("MethodParameterOfConcreteClass")
public interface ConnectionManager {
    Connection getConnectionFor(final DataSource dataSource) throws SQLException;
}
