package com.silensoft.conflated.data.service.sortby;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class SortBy {
    private @NotNull(message = "expression is mandatory field") String expression;

    private @NotNull(message = "type is mandatory field") String type;

    private @NotNull(message = "timeSortOption is mandatory field") String timeSortOption;

    private @NotNull(message = "sortDirection is mandatory field") String sortDirection;

    public String getExpression() {
        return expression;
    }

    public void setExpression(final String newExpression) {
        expression = newExpression;
    }

    public String getType() {
        return type;
    }

    public void setType(final String newType) {
        type = newType;
    }

    public String getTimeSortOption() {
        return timeSortOption;
    }

    public void setTimeSortOption(final String newTimeSortOption) {
        timeSortOption = newTimeSortOption;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(final String newSortDirection) {
        sortDirection = newSortDirection;
    }

    public String getSqlOrderByExpression(final int sortByIndex) {
        final StringBuilder sqlOrderByExpressionBuilder = new StringBuilder(128);

        if (sortByIndex != 0) {
            sqlOrderByExpressionBuilder.append(", ");
        }

        sqlOrderByExpressionBuilder.append(expression);
        sqlOrderByExpressionBuilder.append(' ');
        sqlOrderByExpressionBuilder.append(sortDirection);

        return sqlOrderByExpressionBuilder.toString();
    }
}
