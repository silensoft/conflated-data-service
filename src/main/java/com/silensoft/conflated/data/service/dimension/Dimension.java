package com.silensoft.conflated.data.service.dimension;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class Dimension {
    private @NotNull(message = "name is mandatory field") String name;

    private @NotNull(message = "isTimestamp is mandatory field") Boolean isTimestamp;

    private @NotNull(message = "isDate is mandatory field") Boolean isDate;

    private @NotNull(message = "isString is mandatory field") Boolean isString;

    public String getName() {
        return name;
    }

    public void setName(final String newName) {
        name = newName;
    }

    public boolean isTimestamp() {
        return isTimestamp;
    }

    public void setTimestamp(final boolean newIsTimestamp) {
        isTimestamp = newIsTimestamp;
    }

    public boolean isDate() {
        return isDate;
    }

    public void setDate(final boolean newIsDate) {
        isDate = newIsDate;
    }

    public boolean isString() {
        return isString;
    }

    public void setString(final boolean newIsString) {
        isString = newIsString;
    }
}