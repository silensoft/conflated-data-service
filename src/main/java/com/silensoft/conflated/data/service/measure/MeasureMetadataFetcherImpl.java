package com.silensoft.conflated.data.service.measure;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("LocalVariableOfConcreteClass")
@Stateless
public class MeasureMetadataFetcherImpl implements MeasureMetadataFetcher {

    @Override
    public List<Measure> fetchMeasures(final Connection connection, final String sqlStatement) throws SQLException {
        final ResultSetHandler<List<Measure>> resultSetHandler = resultSet -> {
            if (!resultSet.next()) {
                return Collections.emptyList();
            }

            final ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            final int columnCount = resultSetMetaData.getColumnCount();
            final List<Measure> measures = new ArrayList<>(columnCount);

            for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
                final int columnType = resultSetMetaData.getColumnType(columnIndex + 1);
                if (columnType == Types.FLOAT || columnType == Types.DOUBLE || columnType == Types.DECIMAL) {
                    //noinspection ObjectAllocationInLoop
                    final Measure measure = new Measure();
                    measure.setName(resultSetMetaData.getColumnName(columnIndex + 1));
                    measures.add(measure);
                }
            }
            return measures;
        };

        final QueryRunner queryRunner = new QueryRunner();
        return queryRunner.query(connection, sqlStatement, resultSetHandler);
    }
}
