package com.silensoft.conflated.data.service;

import com.silensoft.conflated.data.service.dataquery.DataQueryService;
import com.silensoft.conflated.data.service.dimension.DimensionService;
import com.silensoft.conflated.data.service.measure.MeasureService;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("v1")
public class ConflatedDataServiceApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> resources = new HashSet<>(1);

        resources.add(DataQueryService.class);
        resources.add(MeasureService.class);
        resources.add(DimensionService.class);
        resources.add(ConstraintViolationExceptionMapper.class);

        return resources;
    }
}
