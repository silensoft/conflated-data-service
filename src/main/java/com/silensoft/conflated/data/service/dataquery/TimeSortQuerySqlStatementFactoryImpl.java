package com.silensoft.conflated.data.service.dataquery;

import com.silensoft.conflated.data.service.column.Column;
import com.silensoft.conflated.data.service.sortby.SortBy;

import javax.ejb.Stateless;

@Stateless
public class TimeSortQuerySqlStatementFactoryImpl implements TimeSortQuerySqlStatementFactory {
    private static final String LATEST_VALUE = "Latest value";
    private static final String LATEST_INCREASE = "Latest increase";
    private static final String LATEST_DECREASE = "Latest decrease";
    private static final String HISTORICAL_MINIMUM = "Historical minimum";
    private static final String HISTORICAL_MAXIMUM = "Historical maximum";
    private static final String HISTORICAL_AVERAGE = "Historical average";
    private static final String HISTORICAL_STDDEV = "Historical stddev";
    private static final String HISTORICAL_INCREASE = "Historical increase";
    private static final String HISTORICAL_DECREASE = "Historical decrease";
    private static final String HISTORICAL_MAX_CHANGE = "Historical max increase";
    private static final String INSTANTANEOUS_INCREASE_IN_HISTORY = "Instantaneous increase in history";
    private static final String INSTANTANEOUS_DECREASE_IN_HISTORY = "Instantaneous decrease in history";

    @SuppressWarnings("MethodParameterOfConcreteClass")
    @Override
    public String createTimeSortQuerySqlStatement(final String sqlFromExpression, final SortBy timeSortBy, final Column firstDimensionColumn, final Column secondDimensionColumn, final Column firstMeasureColumn) {
        if (LATEST_VALUE.equals(timeSortBy.getTimeSortOption())) {
            return String.format("SELECT DISTINCT %s, FIRST_VALUE(%s) OVER (PARTITION BY %s ORDER BY %s DESC) AS last_measure___ %s ORDER BY last_measure___ %s LIMIT 10",
                    firstDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    secondDimensionColumn.getName(),
                    sqlFromExpression,
                    timeSortBy.getSortDirection());
        } else if (LATEST_INCREASE.equals(timeSortBy.getTimeSortOption())) {
            return String.format("SELECT DISTINCT %s, NTH_VALUE(%s, 1) OVER (PARTITION BY %s ORDER BY %s DESC) - NTH_VALUE(%s, 2) OVER (PARTITION BY %s ORDER BY %s DESC) AS latest_increase___ %s ORDER BY latest_increase___ %s LIMIT 10",
                    firstDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    secondDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    secondDimensionColumn.getName(),
                    sqlFromExpression,
                    timeSortBy.getSortDirection());
        } else if (LATEST_DECREASE.equals(timeSortBy.getTimeSortOption())) {
            return String.format("SELECT DISTINCT %s, NTH_VALUE(%s, 2) OVER (PARTITION BY %s ORDER BY %s DESC) - NTH_VALUE(%s, 1) OVER (PARTITION BY %s ORDER BY %s DESC) AS latest_decrease___ %s ORDER BY latest_decrease___ %s LIMIT 10",
                    firstDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    secondDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    secondDimensionColumn.getName(),
                    sqlFromExpression,
                    timeSortBy.getSortDirection());
        } else if (HISTORICAL_MINIMUM.equals(timeSortBy.getTimeSortOption())) {
            return String.format("SELECT DISTINCT %s, MIN(%s) OVER (PARTITION BY %s) AS min_value___ %s ORDER BY min_value___ %s LIMIT 10",
                    firstDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    sqlFromExpression,
                    timeSortBy.getSortDirection());
        } else if (HISTORICAL_MAXIMUM.equals(timeSortBy.getTimeSortOption())) {
            return String.format("SELECT DISTINCT %s, MAX(%s) OVER (PARTITION BY %s) AS max_value___ %s ORDER BY max_value___ %s LIMIT 10",
                    firstDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    sqlFromExpression,
                    timeSortBy.getSortDirection());
        } else if (HISTORICAL_AVERAGE.equals(timeSortBy.getTimeSortOption())) {
            return String.format("SELECT DISTINCT %s, AVG(%s) OVER (PARTITION BY %s) AS avg_value___ %s ORDER BY avg_value___ %s LIMIT 10",
                    firstDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    sqlFromExpression,
                    timeSortBy.getSortDirection());
        } else if (HISTORICAL_STDDEV.equals(timeSortBy.getTimeSortOption())) {
            return String.format("SELECT DISTINCT %s, STDDEV(%s) OVER (PARTITION BY %s) AS stddev_value___ %s ORDER BY stddev_value___ %s LIMIT 10",
                    firstDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    sqlFromExpression,
                    timeSortBy.getSortDirection());
        } else if (HISTORICAL_INCREASE.equals(timeSortBy.getTimeSortOption())) {
            return String.format("SELECT DISTINCT %s, FIRST_VALUE(%s) OVER (PARTITION BY %s ORDER BY %s DESC) - FIRST_VALUE(%s) OVER (PARTITION BY %s ORDER BY %s ASC) AS historical_increase___ %s ORDER BY historical_increase___ %s LIMIT 10",
                    firstDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    secondDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    secondDimensionColumn.getName(),
                    sqlFromExpression,
                    timeSortBy.getSortDirection());
        } else if (HISTORICAL_DECREASE.equals(timeSortBy.getTimeSortOption())) {
            return String.format("SELECT DISTINCT %s, FIRST_VALUE(%s) OVER (PARTITION BY %s ORDER BY %s ASC) - FIRST_VALUE(%s) OVER (PARTITION BY %s ORDER BY %s DESC) AS historical_decrease___ %s ORDER BY historical_decrease___ %s LIMIT 10",
                    firstDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    secondDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    secondDimensionColumn.getName(),
                    sqlFromExpression,
                    timeSortBy.getSortDirection());
        } else if (HISTORICAL_MAX_CHANGE.equals(timeSortBy.getTimeSortOption())) {
            return String.format("SELECT DISTINCT %s, MAX(%s) OVER (PARTITION BY %s) - MIN(%s) OVER (PARTITION BY %s) AS historical_max_change___ %s ORDER BY historical_max_change___ %s LIMIT 10",
                    firstDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    sqlFromExpression,
                    timeSortBy.getSortDirection());
        } else if (INSTANTANEOUS_INCREASE_IN_HISTORY.equals(timeSortBy.getTimeSortOption())) {
            return String.format("SELECT DISTINCT %s, LEAD(%s, 0) OVER (PARTITION BY %s ORDER BY %s DESC) - LEAD(%s, 1) OVER (PARTITION BY %s ORDER BY %s DESC) AS instantaneous_increase_in_history___ %s ORDER BY instantaneous_increase_in_history___ %s LIMIT 10",
                    firstDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    secondDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    secondDimensionColumn.getName(),
                    sqlFromExpression,
                    timeSortBy.getSortDirection());
        } else if (INSTANTANEOUS_DECREASE_IN_HISTORY.equals(timeSortBy.getTimeSortOption())) {
            return String.format("SELECT DISTINCT %s, LEAD(%s, 1) OVER (PARTITION BY %s ORDER BY %s DESC) - LEAD(%s, 0) OVER (PARTITION BY %s ORDER BY %s DESC) AS instantaneous_decrease_in_history___ %s ORDER BY instantaneous_decrease_in_history___ %s LIMIT 10",
                    firstDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    secondDimensionColumn.getName(),
                    firstMeasureColumn.getName(),
                    firstDimensionColumn.getName(),
                    secondDimensionColumn.getName(),
                    sqlFromExpression,
                    timeSortBy.getSortDirection());
        }

        return "";
    }
}
