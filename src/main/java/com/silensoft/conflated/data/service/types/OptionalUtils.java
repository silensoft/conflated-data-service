package com.silensoft.conflated.data.service.types;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class OptionalUtils {
    private OptionalUtils() {
    }

    public static <T> void ifAllPresent(final Collection<Optional<T>> optionals, final ListConsumer<T> listConsumer) {
        final boolean areAllPresent = optionals.stream().allMatch(Optional::isPresent);

        if (areAllPresent) {
            final List<T> values = optionals.stream().map(Optional::get).collect(Collectors.toList());
            listConsumer.consume(values);
        }
    }
}
