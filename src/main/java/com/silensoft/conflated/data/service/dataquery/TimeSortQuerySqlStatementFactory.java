package com.silensoft.conflated.data.service.dataquery;

import com.silensoft.conflated.data.service.column.Column;
import com.silensoft.conflated.data.service.sortby.SortBy;

public interface TimeSortQuerySqlStatementFactory {
    @SuppressWarnings("MethodParameterOfConcreteClass")
    String createTimeSortQuerySqlStatement(final String sqlFromExpression, final SortBy timeSortBy, final Column firstDimensionColumn, final Column secondDimensionColumn, final Column firstMeasureColumn);
}
