package com.silensoft.conflated.data.service.measure;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface MeasureMetadataFetcher {
    List<Measure> fetchMeasures(final Connection connection, final String sqlStatement) throws SQLException;
}
