package com.silensoft.conflated.data.service.connection;

import com.silensoft.conflated.data.service.datasource.DataSource;
import com.silensoft.conflated.data.service.util.PasswordDecryptor;
import org.apache.commons.dbcp.BasicDataSource;

import javax.ejb.Singleton;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Singleton
public class ConnectionManagerImpl implements ConnectionManager {
    private static final Logger logger = Logger.getLogger("ConnectionManagerImpl");

    @Inject
    private PasswordDecryptor passwordDecryptor;

    private final Map<String, BasicDataSource> jdbcUrlToBasicDataSourceMap = new HashMap<>(10);

    @SuppressWarnings({"MethodParameterOfConcreteClass", "FeatureEnvy"})
    public Connection getConnectionFor(final DataSource dataSource) throws SQLException {
        if (jdbcUrlToBasicDataSourceMap.containsKey(dataSource.getJdbcUrl())) {
            return jdbcUrlToBasicDataSourceMap.get(dataSource.getJdbcUrl()).getConnection();
        } else {
            try {
                Class.forName(dataSource.getJdbcDriverClass());
                final BasicDataSource basicDataSource = new BasicDataSource();
                basicDataSource.setUrl(dataSource.getJdbcUrl());
                basicDataSource.setUsername(dataSource.getUserName());
                final String decryptedPassword = passwordDecryptor.decryptPassword(dataSource.getPassword());
                basicDataSource.setPassword(decryptedPassword);
                jdbcUrlToBasicDataSourceMap.put(dataSource.getJdbcUrl(), basicDataSource);
                return basicDataSource.getConnection();
            } catch (final ClassNotFoundException exception) {
                throw new ConnectionManagerException();
            }
        }
    }
}
