package com.silensoft.conflated.data.service.datasource;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.Locale;

@Entity
public class DataSource  {
    private @NotNull(message = "name is mandatory field") String name;

    private @NotNull(message = "jdbcDriverClass is mandatory field") String jdbcDriverClass;

    private @NotNull(message = "jdbcUrl is mandatory field") String jdbcUrl;

    private @NotNull(message = "userName is mandatory field") String userName;

    private @NotNull(message = "password is mandatory field") String password;

    private @NotNull(message = "sqlStatement is mandatory field") String sqlStatement;

    private @NotNull(message = "shouldCreateView is mandatory field") boolean shouldCreateView;

    public String getName() {
        return name;
    }

    public void setName(final String newName) {
        name = newName;
    }

    public String getJdbcDriverClass() {
        return jdbcDriverClass;
    }

    public void setJdbcDriverClass(final String newJdbcDriverClass) {
        jdbcDriverClass = newJdbcDriverClass;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(final String newJdbcUrl) {
        jdbcUrl = newJdbcUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String newUserName) {
        userName = newUserName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String newPassword) {
        password = newPassword;
    }

    public String getSqlStatement() {
        return sqlStatement;
    }

    public void setSqlStatement(final String newSqlStatement) {
        sqlStatement = newSqlStatement;
    }

    public boolean getShouldCreateView() {
        return shouldCreateView;
    }

    public void setShouldCreateView(final boolean newShouldCreateView) {
        shouldCreateView = newShouldCreateView;
    }

    public String getSqlFromAndStaticWhereParts() {
        final StringBuilder sqlFromAndStaticWhereParts = new StringBuilder(1024);

        if (shouldCreateView) {
            sqlFromAndStaticWhereParts.append(" FROM Conflated___.\"");
            sqlFromAndStaticWhereParts.append(name);
            sqlFromAndStaticWhereParts.append('\"');
        } else {
            sqlFromAndStaticWhereParts.append(" FROM");
            sqlFromAndStaticWhereParts.append(getSqlFromExpression());

            final String dataSourceSqlWhereExpression = getSqlStaticWhereExpression();

            if (!dataSourceSqlWhereExpression.isEmpty()) {
                sqlFromAndStaticWhereParts.append(" WHERE(");
                sqlFromAndStaticWhereParts.append(dataSourceSqlWhereExpression);
                sqlFromAndStaticWhereParts.append(')');
            }
        }

        return sqlFromAndStaticWhereParts.toString();
    }

    public boolean hasStaticSqlWhereExpression() {
        return !getSqlStaticWhereExpression().isEmpty();
    }

    private String getSqlFromExpression() {
        final String[] beforeAndAfterFromSqlStatementParts = sqlStatement.toUpperCase(Locale.ENGLISH).split("FROM");

        if (beforeAndAfterFromSqlStatementParts.length == 2) {
            final String[] beforeWhereAndAfterWhereSqlStatementParts = beforeAndAfterFromSqlStatementParts[1].split("WHERE");
            return beforeWhereAndAfterWhereSqlStatementParts[0];
        }

        return "";
    }

    private String getSqlStaticWhereExpression() {
        final String[] beforeAndAfterWhereSqlStatementParts = sqlStatement.toUpperCase(Locale.ENGLISH).split("WHERE");

        if (beforeAndAfterWhereSqlStatementParts.length == 2) {
            final String[] beforeUnionIntersectOrExceptAndAfterUnionIntersectOrExceptSqlStatementParts = beforeAndAfterWhereSqlStatementParts[1].split("UNION|INTERSECT|EXCEPT");
            return beforeUnionIntersectOrExceptAndAfterUnionIntersectOrExceptSqlStatementParts[0];
        }

        return "";
    }
}
