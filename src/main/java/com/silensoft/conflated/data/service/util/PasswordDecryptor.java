package com.silensoft.conflated.data.service.util;

public interface PasswordDecryptor {
    String decryptPassword(final String encryptedPassword);
}
