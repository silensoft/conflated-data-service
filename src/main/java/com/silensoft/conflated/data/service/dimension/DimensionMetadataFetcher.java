package com.silensoft.conflated.data.service.dimension;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface DimensionMetadataFetcher {
    List<Dimension> fetchDimensions(final Connection connection, final String sqlStatement) throws SQLException;
}
