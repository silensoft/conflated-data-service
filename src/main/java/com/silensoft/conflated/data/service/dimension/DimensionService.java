package com.silensoft.conflated.data.service.dimension;

import com.silensoft.conflated.data.service.connection.ConnectionManager;
import com.silensoft.conflated.data.service.datasource.DataSource;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@SuppressWarnings("MethodParameterOfConcreteClass")
@Stateless
@Path("dimensions")
public class DimensionService {
    @Inject
    private ConnectionManager connectionManager;

    @Inject
    private DimensionMetadataFetcher dimensionMetadataFetcher;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDimensions(final @NotNull @Valid DataSource dataSource) throws SQLException {
        try (final Connection connection = connectionManager.getConnectionFor(dataSource)) {
            final List<Dimension> dimensions = dimensionMetadataFetcher.fetchDimensions(connection, dataSource.getSqlStatement());
            return Response.ok().entity(dimensions).build();
        } catch (final SQLException sqlException) {
            return Response.serverError().entity("Database connection failed").build();
        }

    }
}
