package com.silensoft.conflated.data.service.filter;

import com.silensoft.conflated.data.service.Constants;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class Filter {
    private @NotNull(message = "expression is mandatory field") String expression;

    private @NotNull(message = "type is mandatory field") String type;

    public String getExpression() {
        return expression;
    }

    public void setExpression(final String newExpression) {
        expression = newExpression;
    }

    public String getType() {
        return type;
    }

    public void setType(final String newType) {
        type = newType;
    }

    public String getSqlWhereExpression(final int filterIndex) {
        final StringBuilder filterSqlWhereExpression = new StringBuilder(256);

        if (Constants.DIMENSION.equals(type) || "measure-non-aggregated".equals(type)) {
            if (filterIndex != 0) {
                filterSqlWhereExpression.append(" AND ");
            }

            filterSqlWhereExpression.append('(');
            filterSqlWhereExpression.append(expression);
            filterSqlWhereExpression.append(')');
        }

        return filterSqlWhereExpression.toString();
    }

    public String getSqlHavingExpression(final int filterIndex) {
        final StringBuilder sqlHavingExpressionBuilder = new StringBuilder(128);

        if (filterIndex != 0) {
            sqlHavingExpressionBuilder.append(" AND ");
        }

        sqlHavingExpressionBuilder.append('(');
        sqlHavingExpressionBuilder.append(expression);
        sqlHavingExpressionBuilder.append(')');

        return sqlHavingExpressionBuilder.toString();
    }
}