package com.silensoft.conflated.data.service.dataquery;

import com.silensoft.conflated.data.service.datasource.DataSource;

import java.sql.Connection;
import java.sql.SQLException;

@SuppressWarnings("MethodParameterOfConcreteClass")
public interface DataQueryViewFactory {
    void createDataQueryViewIfNeeded(final Connection connection, final DataSource sqlStatement) throws SQLException;
}
