package com.silensoft.conflated.data.service.util;

import org.jasypt.util.text.AES256TextEncryptor;

import javax.ejb.Stateless;
import java.util.logging.Logger;

@Stateless
public class JasyptPasswordDecryptorImpl implements PasswordDecryptor {
    private static final Logger logger = Logger.getLogger("JasyptPasswordDecryptorImpl");

    @Override
    public String decryptPassword(final String encryptedPassword) {
        final AES256TextEncryptor textEncryptor = new AES256TextEncryptor();
        //noinspection CallToSystemGetenv
        final String encryptionPassword = System.getenv("CONFLATED_ENCRYPTION_PASSWORD");
        textEncryptor.setPassword(encryptionPassword);
        return textEncryptor.decrypt(encryptedPassword);
    }
}
