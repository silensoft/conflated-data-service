package com.silensoft.conflated.data.service.dataquery;

import com.silensoft.conflated.data.service.connection.ConnectionManager;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Stateless
@Path("data-query")
public class DataQueryService {
    private static final Logger logger = Logger.getLogger("DataQueryService");

    @Inject
    private ConnectionManager connectionManager;

    @Inject
    private DataQueryExecutor dataQueryExecutor;

    @Inject
    private DataQueryViewFactory dataQueryViewFactory;

    @SuppressWarnings("MethodParameterOfConcreteClass")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response executeDataQuery(final @NotNull @Valid DataQuery dataQuery, @Context final SecurityContext securityContext) {
        try (final Connection connection = connectionManager.getConnectionFor(dataQuery.getDataSource())) {
            logger.info("IsUserRoleUser: " + securityContext.isUserInRole("Admin"));
            dataQueryViewFactory.createDataQueryViewIfNeeded(connection, dataQuery.getDataSource());
            final Map<String, List<Object>> columnNameToValuesMap = dataQueryExecutor.executeQuery(connection, dataQuery);
            return Response.ok().entity(columnNameToValuesMap).build();
        } catch (final SQLException sqlException) {
            return Response.serverError().entity("Database connection failed").build();
        }
    }
}
