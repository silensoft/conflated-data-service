package com.silensoft.conflated.data.service.dataquery;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Stateless
public class DataQueryExecutorImpl implements DataQueryExecutor {
    private static final Logger logger = Logger.getLogger("DataQueryExecutorImpl");

    @SuppressWarnings("MethodParameterOfConcreteClass")
    @Override
    public Map<String, List<Object>> executeQuery(final Connection connection, final DataQuery dataQuery) throws SQLException {
        final ResultSetHandler<Map<String, List<Object>>> resultSetHandler = resultSet -> {
            final ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            final int columnCount = resultSetMetaData.getColumnCount();
            final Map<String, List<Object>> columnNameToValuesMap = new HashMap<>(1024);

            while (resultSet.next()) {
                for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
                    final String columnName = resultSetMetaData.getColumnName(columnIndex + 1);
                    if (columnNameToValuesMap.containsKey(columnName)) {
                        final List<Object> values = columnNameToValuesMap.get(columnName);
                        values.add(resultSet.getObject(columnIndex + 1));
                    } else {
                        //noinspection ObjectAllocationInLoop
                        final List<Object> values = new ArrayList<>(1024);
                        values.add(resultSet.getObject(columnIndex + 1));
                        columnNameToValuesMap.put(columnName, values);
                    }
                }
            }

            return columnNameToValuesMap;
        };

        final QueryRunner queryRunner = new QueryRunner();
        final String dataQuerySqlStatement = dataQuery.getDataQuerySqlStatement();
        logger.info(dataQuerySqlStatement);
        return queryRunner.query(connection, dataQuerySqlStatement, resultSetHandler);
    }
}
