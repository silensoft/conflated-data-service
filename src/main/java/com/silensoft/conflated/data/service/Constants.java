package com.silensoft.conflated.data.service;

public final class Constants {
    public static final String DIMENSION = "dimension";
    public static final String MEASURE = "measure";
    public static final String TIME = "time";

    private Constants() {
    }
}
