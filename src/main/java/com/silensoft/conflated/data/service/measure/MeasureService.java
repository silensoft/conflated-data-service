package com.silensoft.conflated.data.service.measure;

import com.silensoft.conflated.data.service.connection.ConnectionManager;
import com.silensoft.conflated.data.service.datasource.DataSource;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@SuppressWarnings("MethodParameterOfConcreteClass")
@Stateless
@Path("measures")
public class MeasureService {
    @Inject
    private ConnectionManager connectionManager;

    @Inject
    private MeasureMetadataFetcher measureMetadataFetcher;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMeasures(final @NotNull @Valid DataSource dataSource) {
        try (final Connection connection = connectionManager.getConnectionFor(dataSource)) {
            final List<Measure> measures = measureMetadataFetcher.fetchMeasures(connection, dataSource.getSqlStatement());
            return Response.ok().entity(measures).build();
        } catch (final SQLException sqlException) {
            return Response.serverError().entity("Database connection failed").build();
        }
    }
}
